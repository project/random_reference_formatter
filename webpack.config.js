const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = [];
const bc = {
  mode: 'production',
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          format: {
            comments: false,
          },
        },
        test: /\.js(\?.*)?$/i,
        extractComments: false,
      }),
    ],
    moduleIds: 'named',
  },
  entry: {
    path: path.resolve(
      __dirname,
      'js',
      'src',
      'randomEntity.js',
    ),
  },
  output: {
    path: path.resolve(__dirname, './js/build'),
    filename: "randomEntity.js",
    libraryExport: 'default',
  },
  plugins: [],
  module: {},
};

module.exports.push(bc);
