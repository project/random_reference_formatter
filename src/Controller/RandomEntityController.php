<?php

namespace Drupal\random_reference_formatter\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Process the income data and returns a random entities.
 */
class RandomEntityController extends ControllerBase {

  protected ?RendererInterface $renderer;

  public function __construct(RendererInterface $renderer = NULL) {
    $this->renderer = $renderer;
  }

  public static function create(ContainerInterface $container): RandomEntityController|static {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Get the random Entity.
   */
  public function content(Request $request): JsonResponse|Response {
    $requestContent = json_decode($request->getContent());
    $candidatesGeneric = explode(',', $requestContent->ids ?? '');

    if (empty($candidatesGeneric)) {
      return $this->returnError();
    }

    $candidates = [];
    foreach ($candidatesGeneric as $idGeneric) {
      $id = strtok($idGeneric, '/');
      $entityType = strtok('');

      if (empty($idGeneric) || empty($entityType)) {
        continue;
      }

      $candidates[] = [
        'id' => $id,
        'entityType' => $entityType,
      ];
    }

    $viewMode = $requestContent->view_mode ?? 'default';
    $quantity = $requestContent->quantity ?: 1;

    try {

      $output = $dataToLoad = [];

      $quantity = min($quantity, count($candidates));
      shuffle($candidates);
      $randomEntities = array_slice($candidates, 0, $quantity);

      foreach ($randomEntities as $randomEntityData) {
        $dataToLoad[$randomEntityData['entityType']][] = $randomEntityData['id'];
      }

      foreach ($dataToLoad as $entityType => $ids) {

        $entityStorage = $this->entityTypeManager()
          ->getStorage($entityType);
        $entities = $entityStorage->loadMultiple($ids);

        $entityViewBuilder = $this->entityTypeManager()
          ->getViewBuilder($entityType);
        $output += $entityViewBuilder->viewMultiple($entities, $viewMode);

      }

      $randomOutput = !empty($output) ? $this->renderer->render($output) : '';

    } catch (\Exception $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('random_reference_formatter'), $e), fn() => watchdog_exception('random_reference_formatter', $e));
      return new Response('', 500);
    }

    return new JsonResponse(['randomEntities' => $randomOutput]);
  }

  /**
   * Return error.
   *
   * @return JsonResponse
   *   JSON response containing error.
   */
  private function returnError(): JsonResponse {
    return new JsonResponse(['error' => TRUE]);
  }

}
