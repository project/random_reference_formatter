(function (Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.RandomEntity = {
    attach: function (context, settings) {

      let placeholders = document.querySelectorAll('.random-entity-placeholder'), i;
      for (i = 0; i < placeholders.length; ++i) {
        const placeholder = placeholders[i],
          viewMode = placeholder.dataset.entityRandomViewmode,
          quantity = placeholder.dataset.entityRandomQuantity,
          candidates = placeholder.dataset.entityRandomCandidates;

        const options = {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({
            ids: candidates,
            view_mode: viewMode,
            quantity: quantity
          })
        };

        fetch(drupalSettings.randomEntityCallbackURL, options)
          .then(response => response.json())
          .then(data => {
            if (data.randomEntities !== undefined && data.randomEntities !== '') {
              const fieldItems = placeholder.querySelector('.field__items');
              if (fieldItems) {
                fieldItems.innerHTML = data.randomEntities;
              } else {
                placeholder.innerHTML = data.randomEntities;
              }
              placeholder.style.display = '';
            }
          })
          .catch(error => console.error(error));
      }
    }
  };

})(Drupal, drupalSettings);
